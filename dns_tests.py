# !/usr/bin/env python2

""" Tests for your DNS resolver and server """
import socket
import sys
import unittest
from threading import Thread

import dns.message
from dns.classes import Class
from dns.resolver import Resolver
from dns.server import Server
from dns.types import Type


class TestResolver(unittest.TestCase):
    # No caching, existing host
    def test_resolver1(self):
        #  stable/existing host
        hostname = 'gaia.cs.umass.edu'
        res = Resolver(caching=False, ttl=0)
        hostname, aliases, ipaddresses = res.gethostbyname(hostname)
        self.assertEqual(ipaddresses, ['128.119.245.12'])

    # Non-existing host
    def test_resolver2(self):
        #  stable/existing host
        hostname = 'non.existing.host'
        res = Resolver(caching=False, ttl=0)
        hostname, aliases, ipaddresses = res.gethostbyname(hostname)
        self.assertEqual(ipaddresses, [])


class TestResolverCache(unittest.TestCase):
    pass


class TestServer(unittest.TestCase):
    def testServer(self):
        print 'Testing server'
        client_udp_ip = 'localhost'
        server_udp_ip = 'localhost'
        server_udp_port = 6001
        client_udp_port = 6002

        # Set up server in a new thread, because our server is blocking :-)
        def setup_server(port, ttl=0, caching=False):
            s = Server(port, ttl, caching)
            s.serve()

        server_thread = Thread(target=setup_server, args=(server_udp_port,))
        server_thread.daemon = True
        server_thread.start()

        # Forge a DNS request
        hostname = 'gaia.cs.umass.edu'
        header = dns.message.Header(ident=42, flags=0, qd_count=1, an_count=0, ns_count=0, ar_count=0)
        header.rcode = 0
        header.opcode = 0
        header.qr = 0
        header.rd = 1
        questions = [dns.message.Question(qname=hostname, qclass=Class.IN, qtype=Type.A)]
        message = dns.message.Message(header=header, questions=questions)

        # Create client socket and send request
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        client_socket.bind((client_udp_ip, client_udp_port))
        client_socket.settimeout(2)
        client_socket.sendto(message.to_bytes(), (server_udp_ip, server_udp_port))

        # Receive response and check result
        data = client_socket.recv(512)
        try:
            response = dns.message.Message.from_bytes(data)
        except socket.timeout:
            print 'timeout'

        # Check header data
        self.assertEqual(response.header.ident, 42)  # Should match id that was sent
        self.assertEqual(response.header.qd_count, 1)  # Unchanged question count
        self.assertEqual(response.header.an_count, 1)  # Should have 1 answer
        self.assertEqual(response.header.ns_count, 0)
        self.assertEqual(response.header.ar_count, 0)

        # Check body data
        self.assertEqual(len(response.answers), 1)  # Should contain 1 answer
        answer = response.answers[0]
        self.assertEqual(answer.rdata.data, '128.119.245.12')  # Answer should be this ip according to google dns
        self.assertEqual(answer.name, 'gaia.cs.umass.edu')
        self.assertEqual(answer.type_, Type.A)
        self.assertEqual(answer.class_, Class.IN)
        self.assertEqual(len(response.additionals), 0)
        self.assertEqual(len(response.authorities), 0)
        self.assertEqual(len(response.questions), 1)  # Question should remain unchanged in response
        question_recv = response.questions[0]
        question_sent = questions[0]  # We could write a __eq__ method for the Question class,
        self.assertEqual(question_recv.qname, question_sent.qname)  # but since it's only used once we do it like this
        self.assertEqual(question_recv.qtype, question_sent.qtype)
        self.assertEqual(question_recv.qclass, question_sent.qclass)

        # shutdown server, close client socket
        client_socket.close()


if __name__ == "__main__":
    # Parse command line arguments
    import argparse

    parser = argparse.ArgumentParser(description="HTTP Tests")
    parser.add_argument("-s", "--server", type=str, default="localhost")
    parser.add_argument("-p", "--port", type=int, default=5001)
    args, extra = parser.parse_known_args()
    portnr = args.port
    server = args.server

    # Pass the extra arguments to unittest
    sys.argv[1:] = extra

    # Start test suite
    unittest.main()
