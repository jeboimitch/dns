# !/usr/bin/env python2

""" DNS Resolver

This module contains a class for resolving hostnames. You will have to implement
things in this module. This resolver will be both used by the DNS client and the
DNS server, but with a different list of servers.
"""

import socket

from dns.classes import Class
from dns.types import Type

import dns.cache
import dns.message
import dns.rcodes


class Resolver(object):
    """ DNS resolver """

    def __init__(self, caching=False, ttl=-1):
        """ Initialize the resolver

        Args:
            caching (bool): caching is enabled if True
            ttl (int): ttl of cache entries (if > 0)
        """
        self.caching = caching
        self.ttl = ttl

        # root hints
        self.root_hints = ["198.41.0.4", "192.228.79.201", "192.33.4.12", "199.7.91.13", "192.203.230.10", "192.5.5.241",
                     "192.112.36.4", "198.97.190.53", "192.36.148.17", "192.58.128.30", "193.0.14.129", "199.7.83.42",
                     "202.12.27.33"]

    def gethostbyname(self, hostname):
        """ Translate a host name to IPv4 address.

        Currently this method contains an example. You will have to replace
        this example with example with the algorithm described in section
        5.3.3 in RFC 1034.

        Args:
            hostname (str): the hostname to resolve

        Returns:
            (str, [str], [str]): (hostname, aliaslist, ipaddrlist)
        """

        # make an udp socket
        timeout = 5
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.settimeout(timeout)

        aliases = []
        addresses = []
        hints = self.root_hints
        aa_found = False  # search till an authoritative answer is found

        while hints and not addresses: # keep searching until either 1) hints is exhausted 2) an address was found
            server = hints.pop(0)

            # create query
            question = dns.message.Question(hostname, Type.A, Class.IN)
            header = dns.message.Header(9001, 0, 1, 0, 0, 0)
            header.qr = 0   # since it's a request
            header.opcode = 0
            header.rd = 0  # recursive is false, since the request is iterative
            query = dns.message.Message(header, [question])

            # send query to server
            sock.sendto(query.to_bytes(), (server, 53))

            # wait for response, still blocking non-concurrent
            data = sock.recv(512)
            response = dns.message.Message.from_bytes(data)

            # process the response
            if response.header.rcode == 0 and response.header.qr:  # no error and it is a reponse
                if response.header.aa == 1024:  # authoritative answer, aa-bit is not parsed correctly we think (1 is shifted left by 10 bits)
                    addresses = addresses + [a.rdata.data for a in response.answers if a.type_ == Type.A]
                    aliases = aliases + [a.rdata.data for a in response.additionals if a .type_ == Type.CNAME]
                else:  # non authoritative answer aka referral
                    hints = [a.rdata.data for a in response.additionals if a.type_ == Type.A] + hints

        sock.close()
        return hostname, aliases, addresses