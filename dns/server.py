#!/usr/bin/env python2

""" A recursive DNS server

This module provides a recursive DNS server. You will have to implement this
server using the algorithm described in section 4.3.2 of RFC 1034.
"""

from threading import Thread
from dns.classes import Class
from dns.types import Type
import socket
import dns.message
import dns.resource
from dns.resolver import Resolver


class RequestHandler(Thread):
    """ A handler for requests to the DNS server """

    def __init__(self, request, client):
        """ Initialize the handler thread """
        super(RequestHandler, self).__init__()
        self.daemon = True
        self.request = request
        self.client = client

    def run(self):
        """ Run the handler thread """
        # Handle DNS request
        print 'got a request from %s' % (self.client,)

        # TODO: load zone file & check if occurrence
        # if in zone
        # create dns message, set aa to 1

        # TODO: What in case of multiple questions?
        for q in self.request.questions:
            if q.qtype == Type.A and q.qclass == Class.IN:
                hostname = q.qname
                resolver = Resolver()
                hostname, aliases, addresses = resolver.gethostbyname(hostname)

        # Construct header and set header flags
        header = dns.message.Header(ident=self.request.header.ident,
                                    flags=0,
                                    qd_count=self.request.header.qd_count,
                                    an_count=len(addresses),
                                    ns_count=0,
                                    ar_count=0)

        if addresses and aliases:               # success
            header.rcode = 0
        else:                                   # not found in domain
            header.rcode = 3
        header.opcode = 0                       # query
        header.qr = 1                           # response
        header.rd = self.request.header.rd
        header.ra = 1                           # our server is recursive
        header.aa = 0                           # 0 because our dns server is not authoritative in this case

        # Construct body of dns message
        answers = []
        for a in addresses:
            rdata = dns.resource.ARecordData.create(Type.A, a)
            answers.append(dns.resource.ResourceRecord(hostname, Type.A, Class.IN, 0, rdata))

        # Create the response
        response = dns.message.Message(header, self.request.questions, answers, [], [])

        # Answer client
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(response.to_bytes(), self.client)


class Server(object):
    """ A recursive DNS server """

    def __init__(self, port, caching, ttl):
        """ Initialize the server

        Args:
            port (int): port that server is listening on
            caching (bool): server uses resolver with caching if true
            ttl (int): ttl for records (if > 0) of cache
        """
        self.caching = caching
        self.ttl = ttl
        self.port = port
        self.done = False

        # Create socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(('localhost', port))
        self.sock.settimeout(2)

    def serve(self):
        """ Start serving request """
        # Start listening
        while not self.done:
            # Receive request and open handler
            try:
                message, client = self.sock.recvfrom(512)
                request = dns.message.Message.from_bytes(message)
                rh = RequestHandler(request, client).start()
            except socket.timeout:
                pass

    def shutdown(self):
        """ Shutdown the server """
        self.done = True
        # Shutdown socket
        self.sock.close()
